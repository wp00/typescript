let w: unknown = 1;
w = "String";
w = {
    runANonExisttentMethod: () => {
        console.log("I think therefore I am");
    }
} as { runANonExisttentMethod: () => void }

if(typeof w === 'object' && w!== null) {
    (w as { runANonExisttentMethod: () => void }).runANonExisttentMethod();
}